// BÀI 1
document.getElementById("btnSapXep").onclick = function () {
  // Input: soThu1, soThu2, soThu3: number
  var soThu1 = Number(document.getElementById("soThu1").value);
  var soThu2 = Number(document.getElementById("soThu2").value);
  var soThu3 = Number(document.getElementById("soThu3").value);

  // Output: số theo thứ tự tăng dần: string
  soSapXep = "";

  // Progress:
  if (soThu1 > soThu2 && soThu1 > soThu3) {
    if (soThu2 > soThu3) {
      soSapXep = `${soThu3}, ${soThu2}, ${soThu1}`;
    } else {
      soSapXep = `${soThu2}, ${soThu3}, ${soThu1}`;
    }
  } else if (soThu2 > soThu1 && soThu2 > soThu3) {
    if (soThu1 > soThu3) {
      soSapXep = `${soThu3}, ${soThu1}, ${soThu2}`;
    } else {
      soSapXep = `${soThu1}, ${soThu3}, ${soThu2}`;
    }
  } else if (soThu3 > soThu1 && soThu3 > soThu2) {
    if (soThu1 > soThu2) {
      soSapXep = `${soThu2}, ${soThu1}, ${soThu3}`;
    } else {
      soSapXep = `${soThu1}, ${soThu2}, ${soThu3}`;
    }
  }

  //   In kết quả ra giao diện
  document.getElementById("sapXep").innerHTML = "Kết quả = " + soSapXep;
};

// BÀI 2:
document.getElementById("btnChaoHoi").onclick = function () {
  //Input: value bố, mẹ, anhtrai, em gái;
  var chaoHoi = Number(document.getElementById("thanhVien").value);

  // Output: string
  var loiChao = "";

  // Progress:
  switch (chaoHoi) {
    case 1:
      {
        loiChao = "Xin chào bố!";
      }
      break;
    case 2:
      {
        loiChao = "Xin chào mẹ!";
      }
      break;
    case 3:
      {
        loiChao = "Xin chào anh trai!";
      }
      break;
    case 4:
      {
        loiChao = "Xin chào em gái!";
      }
      break;
    default: {
      loiChao = "Xin chào người lạ!";
    }
  }

  //   In Kết quả ra giao diện
  document.getElementById("chaoHoi").innerHTML = loiChao;
};

// BÀI 3:
document.getElementById("btnDemSo").onclick = function () {
  // Input: 3 số nguyên
  var soNguyen1 = Number(document.getElementById("so_Thu1").value);
  var soNguyen2 = Number(document.getElementById("so_Thu2").value);
  var soNguyen3 = Number(document.getElementById("so_Thu3").value);

  // Output: số chẵn và số lẻ
  var tongSoChan = 0;
  var tongSoLe = 0;
  var soLe = 0;

  //   Tạo biến chứa số chẵn đếm được
  var soChan = 0;

  // Progress:
  // Kiểm tra các số nguyên 1 , 2 , 3 số nào chẵn số nào lẻ
  if (soNguyen1 % 2 == 0) {
    soNguyen1 = soChan;
    ++soNguyen1;
  } else {
    soNguyen1 = soLe;
  }

  if (soNguyen2 % 2 == 0) {
    soNguyen2 = soChan;
    ++soNguyen2;
  } else {
    soNguyen2 = soLe;
  }

  if (soNguyen3 % 2 == 0) {
    soNguyen3 = soChan;
    ++soNguyen3;
  } else {
    soNguyen3 = soLe;
  }

  //  Tính tổng số chẵn và tổng số lẻ 
  //  Output
  tongSoChan = soNguyen1 + soNguyen2 + soNguyen3;
  tongSoLe = 3 - tongSoChan;

  //   In kết quả ra giao diện
  document.getElementById('soChanLe').innerHTML = "Kết quả: Có " + tongSoChan + " số chẵn, và " + tongSoLe + " số lẻ." 
};

// Bài 4:
document.getElementById('btnTamGiac').onclick = function () {
    // Input: nhập 3 cạnh tam giác và ép kiểu dữ liệu thành number
    var canh1 = Number(document.getElementById('canhTamGiac1').value);
    var canh2 = Number(document.getElementById('canhTamGiac2').value);
    var canh3 = Number(document.getElementById('canhTamGiac3').value);

    // Output: string
    canhTamGiac = '';
    hopLe = 1;
    khongHopLe = 0;

    // Progress:
    // Kiểm tra 3 cạnh của tam giác, và in kết quả ra giao diện
    if (canh1 + canh2 > canh3 && canh1 + canh3 > canh2 && canh2 + canh3 > canh1) {
        if (canh1 == canh2 && canh1 == canh3 && canh3 == canh2){
            document.getElementById('tamGiac').innerHTML = 'Kết quả: Đây là tam giác đều';
        }else if (canh1 == canh2 || canh1 == canh3 || canh3 == canh2){
            document.getElementById('tamGiac').innerHTML = 'Kết quả: Đây là tam giác cân';
        }else if(canh3*canh3 == canh1*canh1 + canh2*canh2){
            document.getElementById('tamGiac').innerHTML = 'Kết quả: Đây là tam giác vuông';
        }else{
            document.getElementById('tamGiac').innerHTML = 'Kết quả: Thuộc một tam giác khác';
        }
    } else{
        document.getElementById('tamGiac').innerHTML = 'Dữ liệu không hợp lệ!';
    }
}